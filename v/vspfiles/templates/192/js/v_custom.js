if (typeof(jQuery) == 'undefined') {
	var filename = '//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js',
		jqs = document.createElement('script');
    jqs.setAttribute("type","text/javascript");
	jqs.setAttribute("src", filename);
	document.getElementsByTagName('head')[0].appendChild(jqs);
}
var TEMPLATEPATH = '/v/vspfiles/templates/192/',
	vjsCartLayout = function() {
	VJS.v65Cart.Layout.zebraStripes('#eee')
						.moveRemoveButtons()
						.moveArticleText()
						.dividerAfterItems()
						.swapCheckoutPaths()
						.swapQTYandPrice()
						.moveCouponForm()
						.moveRecalcTotal()
						.addHeader('Your Cart')
						.formatCellSpacing(2)
						.init();
	if(jQuery('#v65-cart-shipping-details-wrapper').length > 0) {  VJS.v65Cart.Layout.formatShipping(TEMPLATEPATH); }
	
}, vjsCheckoutLayout = function() {
	jQuery(function() { 
		VJS.v65Checkout.Layout.header('Checkout').init();
		if(typeof showCartTable == 'function') {
			showCartTable();
		}
	});
}


var loaderJS = TEMPLATEPATH+'js/vjs-toolkit.js?t='+new Date().getTime();
window.VJS = window.VJS || {};
VJS.Toolkit = { 
	injectTag: function (tagType,path,cacheState,callback) {
			if(cacheState == false || !cacheState) {
				path = path+'?t='+new Date().getTime(); 
			}
			if(tagType == 'css') {
				if (document.createStyleSheet) {
					document.createStyleSheet(path);
					return this;
				} else {
					cssTag = '<link rel="stylesheet" type="text/css" href="'+path+'" />';
					jQuery(document.getElementsByTagName('head')[0]).append(cssTag);
					return this;
				}
			} else if(tagType == 'js') {
				jQuery.getScript(path,function() {
					if(typeof callback != 'undefined') { callback(); }
					return this;
				});			
			} else {
				return false;
			}
			
	},
	checkPage: function(targetPage) {
		if(typeof PageName == 'function') {
			if (PageName().split('#')[0] == targetPage.toLowerCase()) {
				return true;
			} else {
				if(targetPage.toLowerCase() == 'default.asp') {
					if(PageName().split('#')[0] == '') {
						return true;
					}
					return false;
				}
			}
		}
	}
};
	if(VJS.Toolkit.checkPage('shoppingcart.asp')) {
		jQuery(function() { 
			if(jQuery('#v65-cart-table-container').length > 0) {
				VJS.Toolkit.injectTag('css',TEMPLATEPATH+'css/shoppingcart.css',false);
						   //.injectTag('js',TEMPLATEPATH+'js/vjs-shoppingcart.js',false,vjsCartLayout);
				window.VJS = window.VJS || {};
VJS.v65Cart = { 
	Layout: (function ($) {
		return {	
			zebraStripes: function(colorVal) {
				 colorVal = colorVal || '#ccc';
				 var $v65CartDetailsZebra = $('#v65-cart-table .v65-cart-details-row:even td');
				 var $v65CartDetailsProdImage = $('#v65-cart-table .v65-cart-detail-productimage:even td');
				 $v65CartDetailsZebra.css('background-color', colorVal);
				 $v65CartDetailsProdImage.css('background-color', colorVal);
				 return this;
			},
			moveRemoveButtons: function() {
				var $v65CartRemoveCell = $('#v65-cart-table .v65-cart-item-remove-cell'),
					$v65TotalEstBlank = $('#v65-cart-total-estimate-blank'),
					$v65TaxBlank = $('#v65-cart-table .v65-cart-tax-blank'),
					$v65ShippingBlank = $('#v65-cart-shippingcalc-blank'),
					$v65CartHeaderItems = $('#v65-cart-header-itemdescription'),
					$v65CartHeaderTotals = $('#v65-cart-header-total'),
					$v65TaxParent = $('#v65-cart-table .v65-cart-tax-parent-cell'),
					$v65CartLast = $('#v65-cart-table .v65-cart-details-lastcell'),
					v65TotalEstParent = document.getElementById('v65-cart-total-estimate-parent-cell'),
					v65ShippingParent = document.getElementById('v65-cart-shippingcalc-parent-cell'),
					$v65CartDetailsLast = $('#v65-cart-table .v65-cart-details-lastcell'),
					$v65CartKitQuantity = $('#v65-cart-table .v65-cart-kit-quantity'),
					$v65CartKitFirstTD = $('#v65-cart-table .v65-cart-kit-row td:first'),
					$v65CartKitFirst = $('#v65-cart-table .v65-cart-kit-first'),
					$v65CartKitRows = $('#v65-cart-table .v65-cart-kit-row');

				$v65CartRemoveCell.each(function () {
					var currentCell = $(this);
					$(currentCell).appendTo($(currentCell).parent());
					currentCell = null;
				}).children().wrap('<div class="v65-cart-item-remove-div">');

				$v65CartHeaderItems.attr('colspan', '1');
				$v65CartHeaderTotals.attr('colspan', '1');
				$v65TotalEstBlank.insertAfter(v65TotalEstParent);
				$v65TaxBlank.each(function() { 
					var taxBlank = $(this),
						taxSibling = $(this).siblings('#v65-cart-table .v65-cart-tax-parent-cell');
						taxBlank.insertAfter(taxSibling);
						taxBlank = null;
						taxSibling = null;
				});
				$v65ShippingBlank.insertAfter(v65ShippingParent);
				$v65CartDetailsLast.remove();			
				$v65CartKitFirstTD.appendTo($v65CartKitRows);
				$v65CartKitQuantity.attr('colspan','2').before('<td class="colors_lines v65-cart-details-blank v65-cart-details-cell" width="1"></td>');
				$v65CartKitFirst.remove();
				return this;
			},		
			swapCheckoutPaths: function() { 
			/* swap login and guest checkout */
			if($('#v65-checkout-form-table').length > 0) {
			$('#v65-cart-checkout-table > tbody > tr').each(function() {
				var firstCell = $(this).find('> td:first'),
					middleCell = firstCell.next(),
					lastCell = $(this).find('> td:last');
					lastCell.insertBefore(firstCell);
					firstCell.insertAfter(middleCell);
					firstCell = null;
					middleCell = null;
					lastCell = null;
			});
			$('#v65-checkout-details-guest').append($('#v65-checkout-guest-cell input').css('margin-top','15px'));
			}
				return this;
			},
			swapQTYandPrice: function() { 
				var	$v65CartTableTds = $('#v65-cart-table tbody tr td'),
					$v65QuantityTds = $v65CartTableTds.find('input[id*=Quantity]');
				$v65QuantityTds.parent().parent().each(function() {
					var itemPrice = $(this).next().next();
					$(this).insertAfter(itemPrice);
					itemPrice.insertBefore(itemPrice.prev());
					itemPrice = null;
				});
				var qtyHeader = $('#v65-cart-header-itemdescription').siblings().eq(3),
				    eachHeader = $('#v65-cart-header-itemdescription').siblings().eq(5);
					qtyHeader.insertAfter(eachHeader);
					eachHeader.insertBefore(qtyHeader.prev().prev());
				
				
				return this;
			},
			dividerAfterItems: function() { 
				var hrDivider = '<tr class="v65-divider-hr-row v65-cart-details-row">                                                       				\
												<td class="v65-divider-hr-cell" colspan="11"></td>									\
											  </tr>';
					$('#v65-cart-table .v65-cart-details-row:last').after(hrDivider);
					return this;
			},
			moveCouponForm: function() {
				if($('#v65-cart-coupon-entry-details-div').length > 0) {
					var couponEntryDetails = '<tr id="v65-coupon-table-row">                                                       						\
												<td class="v65-cart-details-blank" id="v65-cart-coupon-entry-blank" colspan="3"></td>                            \
												<td id="v65-cart-coupon-entry-details-container" colspan="3"></td>              \
												<td id="v65-cart-update-total-cell" colspan="5"></td>							\
											  </tr><tr class="v65-divider-hr-row v65-cart-details-row">                                                       				\
												<td class="v65-divider-hr-cell" colspan="11"></td>									\
											  </tr>';
					$('#v65-cart-table .v65-cart-details-row:last').after(couponEntryDetails);
					$('#v65-cart-coupon-entry-details-container').append($('#v65-cart-coupon-entry-details-div'));
					
				}
				return this;
			},
			moveRecalcTotal: function() {
				$v65CouponRow = $('#v65-cart-coupon-entry-details-div'),
				$v65UpdateRow = $('#v65-cart-update-total-cell');
				if($v65CouponRow.length < 1) {
					var couponEntryDetails = '<tr id="v65-update-table-row">                                                       						\
												<td class="v65-cart-details-blank" id="v65-cart-update-blank" colspan="3"></td>                            \
												<td id="v65-cart-update-blank-cell" colspan="3"></td>              \
												<td id="v65-cart-update-total-cell" colspan="5"></td>							\
											  </tr>';
					$('#v65-cart-table .v65-cart-details-row:last').after(couponEntryDetails);
				}
				$('#btnRecalculate').appendTo($v65UpdateRow);
				return this;
			},
			moveArticleText: function() { 
				$v65ShippingDetailIntr = $('#v65-cart-shipping-details-text');
				$('#primaryArticleText').appendTo($v65ShippingDetailIntr);
				return this;
			},
			
			addHeader: function(headerText) {
				if(typeof headerText != 'string') { return this; }
				var $v65MoreItems = $('#v65-cart-moreItems');
				$v65MoreItems.after('<h2 class="v65-your-cart-title">'+headerText+'</h2>');
				return this;
			},
			
			formatCellSpacing: function(spacing) {
				if(typeof spacing != 'number') { return this; }
				$('#v65-cart-table').attr('cellspacing', spacing);
				return this;
			},
			
			formatLogin: function() {
				if($('#v65-checkout-login-button-cell input[name*="btn_checkout_login"]').length > 0) {
					var	 loginDetails = '<tr id="v65-checkout-form-newrow">                                                      	\
											<td class="v65-cart-details-blank" colspan="4"></td>                                    \
											<td id="v65-cart-checkout-newlogin" colspan="7">                                        \
											<div class="v65-cart-left-label" id="v65-cart-registered-label">                        \
											Registered customer login<br />                                                         \
											<i><small>(optional)</small></i></div>                                                  \
											</td>                                                                                   \
											<td class="v65-cart-details-blank"></td>                                                \
											</tr><tr id="v65-checkout-form-newbutton">                                              \
											<td class="v65-cart-details-blank" colspan="4"></td>                                    \
											<td id="v65-cart-checkout-newbutton-cell" colspan="7">                                  \
											</td>                                                                                   \
											<td class="v65-cart-details-blank"></td>                                                \
											</tr>';
					$(loginDetails).appendTo('#v65-cart-table');
					$('#v65-cart-checkout-newlogin').append($('#v65-checkout-form-table'));
				}
				return this;
			},
			formatShipping: function(TEMPLATEPATH) {
				var shippingDetails = '<tr class="v65-cart-shipping-details-row">                                   \
                                <td class="v65-cart-details-blank" colspan="3"></td>                                \
                                <td id="v65-cart-shipping-details-text"></td>                                       \
                                <td id="v65-cart-shipping-details-container" colspan="7">                           \
                                <div class="v65-cart-left-label" id="v65-cart-shipping-details-label">              \
                                Calculate Shipping<br />                                                            \
                                <i><small>(optional)</small></i></div></td>                                         \
                                <td class="v65-cart-details-blank"></td>                                            \
                               </tr>',
							   $v65ShippingAddressRadioNotFirst = $('#v65-cart-shipping-addresstype input').not(':first');
							   
				$('#v65-cart-table .v65-cart-tax-row:first').before(shippingDetails);
				$('#v65-cart-shipping-details-text').append($('#v65-cart-show-giftoptions-cell div'))
													.siblings('#v65-cart-shipping-details-container')
													.append($('#v65-cart-shipping-details-wrapper')
													.attr('cellpadding', '0'));
													
			    if($('#v65-cart-shipping-details .v65-cart-shipping-details-input-cell select[name=ShippingSpeedChoice]').length == 0) {
				$('#btnRecalculate').clone().attr('src',TEMPLATEPATH+'images/buttons/shipupdate.gif')
									.appendTo($('#v65-cart-shipping-details tbody'))
									.wrap('<tr><td>');			
				}
				$('#v65-cart-post-coupon-blank').text('')
				$v65ShippingAddressRadioNotFirst.before('<br />');
				$('#v65-cart-shipping-details tbody tr:first a').before('<br />');
				return this;
			},
			all: function() {
						this.zebraStripes('#eee')
						.moveRemoveButtons()
						.moveCouponForm()
						.formatShipping()
						.moveArticleText()
						.addHeader('Your Cart')
						.formatCellSpacing(2)
						.init();
			},			
			init: function () {
				return this;
			}
		}
	} (jQuery))
};

	vjsCartLayout();
			}
		});
	}
	if(VJS.Toolkit.checkPage('one-page-checkout.asp')) {
		jQuery(function() { 
			if(jQuery('#v65-onepage-ContentTable').length > 0) {
				VJS.Toolkit.injectTag('css',TEMPLATEPATH+'css/one-page-checkout.css')
						   //.injectTag('js',TEMPLATEPATH+'js/vjs-one-page-checkout.js',false,vjsCheckoutLayout);
						   window.VJS = window.VJS || {};
VJS.v65Checkout = { 
	Layout: (function ($) {
		return {
			header: function(headerText) {
				// header
				var $v65OnePageContentTable = $('#v65-onepage-ContentTable'),
					headerText = '<h2 id="v65-onepage-header">'+headerText+'</h2>';
				$v65OnePageContentTable.before(headerText);
				return this;
			},
			init: function() {

				var v65CheckoutTable = document.getElementById('table_checkout_cart0'),
					v65BillingParent = document.getElementById('v65-onepage-BillingParent'),
					v65ShippingHeader = document.getElementById('shipping-header'),
					v65ShippingTable = document.getElementById('ShipToSelectTable'),
					v65CheckoutPaymentHeader = document.getElementById('v65-checkout-payment-header'),
					v65OnePageShippingParent = document.getElementById('v65-onepage-ShippingParent');
					v65OnePageShippingCostParent = document.getElementById('v65-onepage-ShippingCostParent'),
					$v65OnePageSavedShipping = $('#v65-onepage-saved-shipping-table'),
					$v65savedSelect = $('#v65-onepage-saved-shipping-table select[name=My_Saved_Shipping]')
					$v65ShippingTable = $('#v65-onepage-saved-shipping-table'),
					$v65ShippingDetails = $('#v65-onepage-Shipping'),
					$v65BillingToShippingLink = $('#v65-onepage-CopyBillingToShippingLink'),
					$v65SavedLocationsShipping = $('#v65-onepage-saved-shipping-table tbody tr td br:first'),
					$v65SavedLocationsBilling = $('#v65-onepage-ContentTable .v65-onepage-SavedLocations .colors_lines_light .colors_backgroundneutral br'),
					$v65ShipToSame = $('#ship-to-options-table select[name=ShipTo]'),
					$v65SavedPayments = $('#savedPayment'),
					$v65SubmitButton = $('#btnSubmitOrder');
				
				/* format a vertical order summary, add totals, comments, optional fields and a checkout button */

				// depending on font size, this first little br can make things pretty awkward.
				$('#v65-onepage-ContentTable br:first').css('line-height','0px');
				
				$('#v65-onepage-registrationpassword-cell').text('Create Password');
				$('#v65-onepage-registrationpassword-confirm-cell').text('Retype Password');
				$('#v65-onepage-ordercomments-value').prepend($('#v65-onepage-ordercomments-label').text()+'<br />').attr('colspan','2');
								
				$('#v65-onepage-ordercomments-label').remove();
				$('#savedPayment').attr('align','left');		
				$('#v65-onepage-ordersummary-items .v65-onepage-ordersummary-itemprice').remove();	
				$('#table_checkout_cart0 td:first').html('<div id="v65-onepage-cartsummary-header"><h2 id="v65-onepage-cartsummary-label">Your Order</h2><a href="shoppingcart.asp" id="v65-onepage-editcart">Edit</a></div>');
				$('#v65-onepage-newsletter-parent').attr('colspan',1).before('<td>');				
				$('#TotalsDivContainer').wrap('<tr id="v65-cart-totals-parent"><td>');
				$('#TotalsWaitDivContainer').wrap('<tr id="v65-cart-wait-totals-parent"><td>');
				$v65SubmitButton.wrap('<tr id="v65-cart-button-submit-row"><td id="v65-cart-button-submit">');
				$('#v65-onepage-ordersummary-header-row .v65-onepage-ordersummary-itemname').text('Item');
				$('#TotalsTotalTD').prepend($('#v65-onepage-total-label-cell').text());
				$('#v65-onepage-total-label-cell').html('');
				
				$(v65CheckoutTable).append($('#v65-cart-totals-parent'))
									.append($('#v65-cart-wait-totals-parent'))
									.append($('#v65-onepage-ordercomments-row'))
									.append($('.v65-onepage-custom-details-row'))
									.append($('#v65-cart-button-submit-row'));
			
			
			
				if($('#span_GiftCertificates_Editable input').length > 2) {
					var v65CheckoutCartLI = $('#font_checkout_cart li').eq(2),
					    v65OrderCommentsRow = $('#v65-onepage-ordercomments-row');
					
					if(v65OrderCommentsRow.length > 1) {
						v65OrderCommentsRow.after('<tr id="v65-apply-gift-newRow"><td id="v65-apply-gift-newCell"></td></tr>');
					} else {
						$('#v65-cart-wait-totals-parent').after('<tr id="v65-apply-gift-newRow"><td id="v65-apply-gift-newCell"></td></tr>');
					}
					
					$('#v65-apply-gift-newCell').append(v65CheckoutCartLI);
					$(v65CheckoutCartLI).after($('#span_GiftCertificates_Editable'))
														.after($('#span_GiftCertificates_UnEditable'));
				}
	
			
			
    			// breadcrumb placement fix - required if floating the order summary.
				$('#v65-onepage-breadcrumb').insertBefore($('#v65-onepage-ContentTable'));	

				/* 
				 *	Shipping adjustments: 
				 *  -- Move shipping form below billing, 
				 *  -- Append shipping method to shipping form
				 *  -- Modify shipping address selection for registered checkout
				*/
				$(v65ShippingHeader).wrap('<tr>');
				$(v65ShippingTable).wrap('<tr>');
				$('#v65-onepage-Detail .v65-onepage-SavedLocations').children().eq(1).insertAfter($('#v65-onepage-Shipping'));
				$(v65OnePageShippingCostParent).before(v65ShippingHeader)
											   .before($v65OnePageSavedShipping)
											   .before(v65ShippingTable)
											   .before(v65OnePageShippingParent);
				if($v65OnePageSavedShipping.length > 0) {
					$('#v65-onepage-ShippingCost').prepend($('#v65-onepage-ShippingParent'));
				} else {
					$('#v65-onepage-ShippingCost').append($('#v65-onepage-ShippingParent'));
				}
				
				$('#v65-onepage-total-label-cell').remove();
				$('#v65-onepage-total-value-cell').attr('colspan','2');
				$v65savedSelect.find('option:first').text('Select');
				$v65savedSelect.append('<option>A New Address</option>');
				var $v65ShippingChoices = $('#DisplayShippingSpeedChoicesTD');
				$v65ShippingChoices.before('<td id="v65-onepage-shipping-method-label">Shipping Method*:&nbsp;</td>');
				if($v65savedSelect.length > 0) { 
				
					if($v65SavedPayments.length > 0) {
					//	$v65SavedPayments.find('td:first').remove().next().attr('colspan','2');
					}
					
					if($v65ShipToSame.length == 0) {
						$v65BillingToShippingLink.hide();
						$v65SavedLocationsShipping.replaceWith('\&nbsp;');
						$v65SavedLocationsBilling.replaceWith('\&nbsp;');
						$v65ShippingDetails.hide();
						$v65ShippingTable.hide();
						$v65ShippingTable.before('<input type="checkbox" id="v65-SameAsBilling" checked="checked"> Ship to my Billing Address');
						$('#v65-SameAsBilling').change(function() { 
							if($(this).attr('checked') == false) {
								$v65ShippingDetails.show();
								$v65ShippingTable.show();
							} else {
								$v65BillingToShippingLink.click();
								$v65ShippingDetails.hide();
								$v65ShippingTable.hide();
							}
						});
										
						$('#CalcShippingDiv input[name=btncalc_shipping]').click(function() { 
							var $v65BillingPref = $('#v65-SameAsBilling');
								if($v65BillingPref.attr('checked') == true) { 
									$v65BillingToShippingLink.click();
								}
							return true;
						});
					
						$v65savedSelect.change(function() { 
							if($v65savedSelect.find('option:selected').text() == 'A New Address') {
								$v65ShippingDetails.find(':input').not(':hidden').val('');
							}
						});

						$v65SubmitButton.click(function() { 
							var $v65BillingPref = $('#v65-SameAsBilling');
							if($v65BillingPref.attr('checked') == true) { 
								$v65BillingToShippingLink.click();
								$v65BillingPref.remove();
							}
							$v65BillingPref = null;
							document.getElementByID('v65-onepage-CheckoutForm').submit();
						});
					}					
				} else {
					// guest checkout presently remains the same.

				}	
				/* cosmetic fix for payment details in IE8 */
				$('#savedPayment').after('<br style="clear:both;" />');
				$('#v65-onepage-CartSummary').css('width','100%');
				$('#v65-onepage-ordersummary-items').css('width','100%');
				$('#v65-cart-header-total table center').css('text-align','right');
				$('#v65-paymenttype-select-parent font:first').appendTo($('#savedPayment td:first')).css('font-weight','normal');
				return this;
			}
		}		
	} (jQuery))
};

			vjsCheckoutLayout();
			}
		});
	}
